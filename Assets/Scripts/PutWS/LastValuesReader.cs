﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class ObjectValue
{
    public int objId;
    public int chId;
    public int valKindId;
    public float value;
    public DateTime dateQuery;
}
public class LastValuesReader : MonoBehaviour {

    public ObjectValue value;
    [HideInInspector]
    public bool isDataReady = false;

    public void Read()
    {
        StartCoroutine("StartReading");
    }

    IEnumerator StartReading()
    {
        isDataReady = false;

        var query = new WWW("http://192.168.254.125:4567/put/1/chanel/1/value/all");

        yield return query;

        if( query.error == null )
        {
            Debug.Log( query.text );
            //todo: parse it
        }
    }
}
